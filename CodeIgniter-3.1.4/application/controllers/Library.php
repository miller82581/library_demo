<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     */
    public function index()
    {
        //similar to viewbags in .net. we are passing data to the views.
        $metaData['title'] = "Welcome to the Library!";

        $this->load->model('library_model');
        $data['results'] = $this->library_model->get_library();
        $this->load->view('templates/header', $metaData);
        $this->load->view('Library', $data);
        $this->load->view('templates/footer');
    }
    public function create()
    {
        //set the title for the page.
        $metaData['title'] = "Create a new book!";
        //loading in helpers to do form validation and some url routing.
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        //setting validation for our form input fields.
        $this->form_validation->set_rules('book_name', 'Book Name', 'trim|required|max_length[50]',
                                array('required' => 'You must enter a %s.',
                                      'max_length' => '%s must be no larger than 50 characters.'));
        $this->form_validation->set_rules('publications', 'Publications', 'trim|required|max_length[50]',
                                array('required' => 'You must enter a %s.',
                                    'max_length' => '%s must be no larger than 50 characters.'));
        $this->form_validation->set_rules('isbn', 'ISBN #', 'trim|required|max_length[13]',
                                array('required' => 'You must enter a %s.',
                                    'max_length' => '%s must be no larger than 13 characters.'));
        $this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[20]',
                                array('required' => 'You must enter a %s.',
                                    'max_length' => '%s must be no larger than 20 characters.'));
        //if we are entering this controller withing posting
        if($this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $metaData);
            $this->load->view('Create');
            $this->load->view('templates/footer');
        }
        else
        {
            //we got to this page via posting
            $this->load->model('library_model');
            //inserting our data
            $this->library_model->insert_entry();
            //displaying the success page.
            $this->load->view('templates/header', $metaData);
            $this->load->view('templates/success');
            $this->load->view('templates/footer');
        }

    }
}