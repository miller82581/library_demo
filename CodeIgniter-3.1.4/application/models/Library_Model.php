<?php

class Library_Model extends CI_Model
{
    public $book_name;
    public $publications;
    public $isbn;
    public $status;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_library()
    {
        //query the database for all the library entries
        $query = $this->db->get('library');

        return $query->result();
    }
    public function insert_entry()
    {
        //set the fields of our model
        $this->book_name = $this->input->post('book_name', true);
        $this->publications = $this->input->post('publications', true);
        $this->isbn = $this->input->post('isbn', true);
        $this->status = $this->input->post('status', true);

        //insert into the database, passing in our model.
        $this->db->insert('library', $this);

    }


}
