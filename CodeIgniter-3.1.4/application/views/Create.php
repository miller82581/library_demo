<h2>Create a New Book</h2>
<div>
    <div style="color:red; font-weight: 800;">
    <?php echo validation_errors(); ?>
    </div>
    <?php echo form_open('index.php/library/create'); ?>

    <div class="form-group">
        <label>Book Name</label>
        <input type="text" name="book_name" class="form-control" value="<?php echo set_value('book_name'); ?>"/>
    </div>

    <div class="form-group">
        <label>Publications</label>
        <input type="text" name="publications" class="form-control" value="<?php echo set_value('publications'); ?>"/>
    </div>

    <div class="form-group">
        <label>ISBN #</label>
        <input type="text" name="isbn" class="form-control" value="<?php echo set_value('isbn'); ?>"/>
    </div>
    <div class="form-group">
        <label>Status</label>
        <input type="text" name="status" class="form-control" value="<?php echo set_value('status'); ?>"/>
    </div>
        <input type="submit" value="Create Book" class="btn btn-default" />
</div>