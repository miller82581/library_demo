
<h1 class="text-center">Welcome to the Library!</h1>

<div class="divTable" style=" border: 2px solid #000;">

    <div class="divTableBody text-center">
        <div class="divTableRow">
            <div class="divTableCell"><strong>Book Name</strong></div>
            <div class="divTableCell"><strong>Publications</strong></div>
            <div class="divTableCell"><strong>ISBN #</strong></div>
            <div class="divTableCell"><strong>Status</strong></div>
        </div>
        <?php foreach($results as $book):?>
        <div class="divTableRow">
            <div class="divTableCell"><?php echo html_escape($book->book_name);?></div>
            <div class="divTableCell"><?php echo html_escape($book->publications);?></div>
            <div class="divTableCell"><?php echo html_escape($book->isbn);?></div>
            <div class="divTableCell"><?php echo html_escape($book->status);?></div>
        </div>
        <?php endforeach;?>
    </div>
</div>